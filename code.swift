// п.1
enum StrRawEnum : String{
    case IOS = "IOS"
    case Android = "Android"
}

enum DoubleRawEnum : Double{
    case Five = 5.0
    case Seven = 7.0
}

// п.2
enum Gender{
    case Man
    case Woman
}

enum AgeCategory{
    case Children
    case Young
    case Adult
    case Old
}

enum Experience{
    case Little
    case Medium
    case Great
}

// п.3
enum Rainbow : CaseIterable{
    case Red
    case Orange
    case Yellow
    case Green
    case LightBlue
    case Blue
    case Violet
}

// п.4
func printRainbow() {
    for color in Rainbow.allCases {
        print(color)
    }
    print()
}

printRainbow()

// п.5
enum Score : Int{
    case Two = 2
    case Three = 3
    case Four = 4
    case Five = 5
}

func printScore(score: Score) {
    print(score.rawValue)
    print()
}

printScore(score: .Five)

// п.6
enum Garage : CaseIterable{
    case Bentley
    case Audi
    case Ford
    case Volga
}

func printGarage() {
    for car in Garage.allCases {
        print(car)
    }
    print()
}

printGarage()